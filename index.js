// import './svg.js'
import './lib/svg.select.js'
import './lib/svg.draggable.js'
import {rgbToHsl, clearBoundingBoxes, makeDraggable, highlightShape, openNav, closeNav } from './util.js'
let swatchComponent = {
  template: '#swatch-template',
  props: {
    color: {
      type: String,
      required: true
    },
    selectedSwatch: {
      type: String
    }
  },
  computed: {
    isSelected () {
      return this.color === this.selectedSwatch
    }
  },
  methods: {
    select () {
      this.$emit('select', this.color)
    }
  }
}

new Vue({
  el: '#app',
  components: {
    swatch: swatchComponent
  },
  data () {
    return {
      'shapebuttons': {
        rectButton: {
          name: 'Rectangle',
          class: 'rect-button',
          shape: `<rect class="rect-button" x="-14" y="-12" width="28" height="24"/>`
        },
        circleButton: {
          name: 'Circle',
          class: 'circle-button',
          shape: `<circle class="circle-button" r="14"/>`
        },
        starButton: {
          name: 'Star',
          class: 'star-button',
          shape: `<polygon class="star-button hover-blue pointer"
                  points='-15,-4.5 -3.8,-4.3 0,-15 3.8,-4.3 15,-4.5 6,3.6 9.2,14.9 0,8.1 -9.2,14.9 -6,3.6'/>`
        },
        diamondButton: {
          name: 'Diamond',
          class: 'diamond-button',
          shape: `<polygon class="diamond-button hover-blue pointer"
                  points='-13,0 0,-15 13,0 0,15'/>`
        },
        downButton: {
          name: 'Downward-Pointing Triangle',
          class: 'down-triangle-button',
          shape: `<polygon class="down-triangle-button hover-blue pointer"
                  points='-15,-13, 15,-13, 0,13'/>`
        },
        upButton: {
          name: 'Upward-Pointing Triangle',
          class: 'up-triangle-button',
          shape: `<polygon class="up-triangle-button hover-blue pointer"
                  points='-15,13, 15,13, 0,-13'/>`
        },
        leftButton: {
          name: 'Left-Pointing Triangle',
          class: 'left-triangle-button',
          shape: `<polygon class="left-triangle-button hover-blue pointer"
                  points='13,-15 13,15 -13,0'/>`
        },
        rightButton: {
          name: 'Right-Pointing Triangle',
          class: 'right-triangle-button',
          shape: `<polygon class="right-triangle-button hover-blue pointer"
                  points='-13,-15 -13,15 13,0'/>`
        }
      },
      thumbnails: {
        bd: { title: 'Bangladesh', code: 'BD' }, fi: { title: 'Finland', code: 'FI' },
        tz: { title: 'Tanzania', code: 'TZ' }, bg: { title: 'Bulgaria', code: 'BG' },
        jp: { title: 'Japan', code: 'JP' }, nl: { title: 'Netherlands', code: 'NL' },
        uk: { title: 'United Kingdom', code: 'UK' }, ca: { title: 'Canada', code: 'CA' },
        ke: { title: 'Kenya', code: 'KE' }, pa: { title: 'Panama', code: 'PA' },
        ur: { title: 'Uruguay', code: 'UR' }, dk: { title: 'Denmark', code: 'DK' },
        mx: { title: 'Mexico', code: 'MX' }, rw: { title: 'Rwanda', code: 'RW' },
        us: { title: 'United States', code: 'US' }, ec: { title: 'Ecuador', code: 'EC' },
        ng: { title: 'Nigeria', code: 'NG' }, tw: { title: 'Taiwan', code: 'TW' },
        nz: { title: 'Aotearoa New Zealand', code: 'NZ' }, au: { title: 'Australia', code: 'AU' },
        id: { title: 'Indonesia', code: 'ID' }
      },
      hexcode: '#e6e6e6',
      selectedSwatch: 'hsl(0, 0%, 90%',
      selectedShape: null,
      upload: '',
      fileName: '',
      shapes: [],
      swatchesArray: [
        'hsl(0, 0%, 100%)', 'hsl(0, 0%, 90%', 'hsl(0, 0%, 80%)',
        'hsl(0, 0%, 20%)', 'hsl(0, 0%, 10%', 'hsl(0, 0%, 0%)'
      ],
      mySVG: '',
      shapeX: null,
      shapeY: null,
      shapeType: '',
      delete: '',
      shapeColor: '',
      mouseX: '',
      mouseY: '',
      xStart: '',
      yStart: '',
      newWidth: '',
      newHeight: '',
      shapePicking: false
    }
  },
  computed: {
    shapeBoundingBox: function () {
      if (this.selectedShape) {
        return this.selectedShape.rbox(this.mySVG)
      }
    },
    width: {
      set: function (value) {
        if (this.selectedShape) {
          this.newWidth = value
        } else {
          this.newWidth = 30
        }
      },
      get: function () {
        return this.newWidth
      }
    },
    height: {
      set: function (value) {
        if (this.selectedShape) {
          this.newHeight = value
        } else {
          this.newHeight = 30
        }
      },
      get: function () {
        return this.newHeight
      }
    }
  },
  methods: {
    shapePicker () {
      if (this.shapePicking === false) {
        this.shapePicking = true
      } else {
        this.shapePicking = false
      }
    },
    createShape () {
      let shape = null
      const button = event.target.classList[0]
      switch (button) {
        case 'rect-button':
          shape = this.mySVG.rect(30, 30).move(-15,-15)
          break
        case 'circle-button':
          shape = this.mySVG.circle(30).move(-15,-15)
          break
        case 'left-triangle-button':
          shape = this.mySVG.polygon('27.7,-32 27.7,32 -28,0')
          break
        case 'right-triangle-button':
          shape = this.mySVG.polygon('-27.7,-32 -27.7,32 28,0')
          break
        case 'down-triangle-button':
          shape = this.mySVG.polygon('-32,-27.7, 32,-27.7, 0,28')
          break
        case 'up-triangle-button':
          shape = this.mySVG.polygon('-32,27.7, 32,27.7, 0,-28')
          break
        case 'diamond-button':
          shape = this.mySVG.polygon('-26,0 0,-28 26,0 0,28')
          break
        case 'star-button':
          shape = this.mySVG.polygon('-15,1.5 -3.8,1.3 0,-10 3.8,1.3 15,1.5 6,8.6 9.2,19.9 0,13.1 -9.2,19.9 -6,8.6')
          break
      }
      this.insertShape(shape)
    },
    insertShape(shape){

      this.clearHighlightedShape(this.selectedShape)
      shape.attr('fill', this.selectedSwatch)
           .addClass('new');
      let clipGroup = SVG.find('#clip-group')[0]
      console.log(clipGroup);
      if (clipGroup) {
        console.log('miaow');

        shape.putIn(clipGroup)
      } else {
        console.log('woof');
        shape.putIn(this.mySVG)
      }
      const addedShape = SVG.find('.new')[0]
      makeDraggable(addedShape)
      addedShape.mousedown(() => this.onShapeClick(shape))
      addedShape.removeClass('new')
      this.shapePicking = false
      this.onShapeClick(shape)
    },
    updateRectSize () {
      if (this.selectedShape) {
        clearBoundingBoxes()
        this.selectedShape.size(this.newWidth, this.height)
        highlightShape(this.selectedShape)
      }
    },
    uploadSVG () {
      let file = document.getElementById('upload').files[0]
      this.fileName = document.getElementById('upload').files[0].name
      let text = new Response(file).text()
      text.then(result => {
        return this.displaySVG(result)
      })
    },

    loadExampleSVG(){
      this.clearHighlightedShape(this.selectedShape)
      let loadRequest = new Request(event.target.src)
      fetch(loadRequest)
      .then(function(response) {
        if (!response.ok) {
          throw new Error('HTTP error, status = ' + response.status);
        }
        return response.text();
      })
      .then(response => {
        return this.displaySVG(response)
      })
    },

    displaySVG(svg) {
      let display = document.getElementById('image')
      display.innerHTML = svg
      let displayedSVG = document.getElementsByTagName('svg')[0]
      displayedSVG.classList.add('editing')
      this.mySVG = SVG.adopt(displayedSVG)
      this.mySVG.attr('width', '100%').attr('height', '100%')
      const paths = SVG.find('path')
      const circles = SVG.find('circle')
      const rects = SVG.find('rect')
      const polygons = SVG.find('polygon')
      const ellipses = SVG.find('ellipse')
      const uses = SVG.find('use')
      const texts = SVG.find('text')
      this.shapes = paths.concat(circles, rects, polygons, ellipses, uses, texts)
      this.clickableShapes()
    },

    clickableShapes () {
      this.shapes.forEach(shape => {
        makeDraggable(shape)
        shape.mousedown(() => this.onShapeClick(shape))
      })
    },

    onShapeClick (shape) {
      this.clearHighlightedShape(this.selectedShape)
      // shape.selectize()
      highlightShape(shape)
      this.selectedShape = shape
      this.width = shape.width()
      this.height = shape.height()
      if (shape.css('fill')) {
        this.shapeColor = shape.css('fill')
      } else {
      this.shapeColor = shape.attr('fill')
      }
    },

    clearHighlightedShape (selectedShape) {
      if (selectedShape != null) {
        selectedShape.removeClass('active-shape')
        this.selectedShape = null
      }
      clearBoundingBoxes()
    },

    deleteShape () {
      if (this.selectedShape != null) {
        const shapeToDelete = this.selectedShape
        this.clearHighlightedShape(this.selectedShape)
        shapeToDelete.remove()
      }
    },

    downloadSVG () {
      let base64Data = btoa(this.mySVG.svg())
      let dataURL = "data:text/html;charset=utf-8;base64," + base64Data
      // let editedFileName = this.fileName.replace(".svg", "") + "-edited.svg"
      let link = document.getElementById('download-link')
      link.href = dataURL
      link.download = 'cc-button.svg'
      link.click()
    },

    clickPicker () {
      const picker = document.getElementById('color-picker')
      picker.focus()
      picker.click()
    },

    addNewColor () {
      const hslValues = this.hexToHsl(this.hexcode)
      let h = hslValues[0]
      let s = hslValues[1]
      let l = hslValues[2]
      let tint10 = Math.min((hslValues[2] + 10), 100)
      let shade10 = Math.max((hslValues[2] - 10), 0)
      let desat10 = Math.max((hslValues[1] - 10), 0)
      let sat10 = Math.min((hslValues[1] + 10), 255)
      let baseColor = this.Hsl(h,s,l)
      let baseTint10 = this.Hsl(h, desat10, tint10)
      let baseShade10 = this.Hsl(h, sat10, shade10)
      if (this.swatchesArray.length === 15) {
        this.swatchesArray.splice(-3)
      }
      return this.swatchesArray.unshift(baseTint10, baseColor, baseShade10)
    },

    selectSwatch (swatch) {
      this.selectedSwatch = swatch
      if (this.selectedShape != null) {
        if (this.selectedShape.css('fill')) {
          this.selectedShape.css('fill', this.selectedSwatch)
        } else {
          this.selectedShape.attr('fill', this.selectedSwatch)
        }
        this.shapeColor = this.selectedSwatch
      }
    },

    hexToHsl () {
      let r = parseInt(this.hexcode.slice(1, 3), 16),
          g = parseInt(this.hexcode.slice(3, 5), 16),
          b = parseInt(this.hexcode.slice(5, 7), 16);
      return rgbToHsl(r, g, b)
    },

    Hsl (h, s, l) {
      return "hsl(" + h + ", " + s + "%, " + l + "%)"
    }
  }
})
