**NOTE:** _The functionality of this project is currently being reimplemented as a multi-purpose SVG remixing tool, with more collaborative features (eg. commenting, easy replacement of elements) and an architecture/setup that will allow the project to grow more easily._

_Follow the work in progress: [SVG-jam](https://gitlab.com/cameralibre/svg-jam/-/boards)_
 
---- 

# CC Buttons

### A customization tool enabling Creative Commons chapters to make their own localized buttons, stickers or graphics.

[Sebastiaan ter Burg](https://twitter.com/ter_burg) from the Creative Commons Netherlands community is getting some CC buttons printed for the [Summit](https://summit.creativecommons.org/) next month:

![](images/CC_Button-NL.png) ![](images/CC_Button-NL_square.png)

He suggested to me that it would be cool if other chapters could make their own versions too.

He provided me with the source file, so I made a bunch of other versions:

![](images/CC_Button-EC.png) ![](images/CC_Button-MX.png) ![](images/CC_Button-KE.png) ![](images/CC_Button-US.png) ![](images/CC_Button-BG.png) ![](images/CC_Button-NG.png) ![](images/CC_Button-TZ.png) ![](images/CC_Button-UR.png) ![](images/CC_Button-TW.png) ![](images/CC_Button-BD.png) ![](images/CC_Button-RW.png) ![](images/CC_Button-UK.png) ![](images/CC_Button-JP.png) ![](images/CC_Button-CA.png) ![](images/CC_Button-FI.png) ![](images/CC_Button-DK.png) ![](images/CC_Button-PA.png) ![](images/CC_Button-NZ.png) ![](images/CC_Button-AU.png)

Some flags work much better in this configuration than others, and legibility can be a problem. Also there are a whole lot of different flag styles...

## Customizable CC Buttons Site

You can currently load an SVG, change its colors, add shapes, resize, move or delete them, and download the SVG.

Short walkthrough videos:

- [creating a color palette, changing colors](https://vimeo.com/329181256)
- [moving and deleting shapes](https://vimeo.com/331140045)
- [adding and resizing shapes](https://vimeo.com/332351912)


The website is under construction, but you can play around with it here:

[**cc-buttons.hashbase.io**](https://cc-buttons.hashbase.io)

![screenshot of website-in-progress](./images/button-customizer.png)

More functionality coming soon...

## TODO

- [ ] enable rotation of shapes
- [ ] make mobile-friendly
- [ ] add an 'undo' function
- [ ] select & adjust multiple shapes
- [ ] basic keyboard shortcuts
