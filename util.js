function rgbToHsl(r, g, b) {
  r = +r/255, g = +g/255, b = +b/255;
  let max = Math.max(r, g, b), min = Math.min(r, g, b);
  let h, s, l = (max + min) / 2;
  if(max == min){
      h = s = 0; // achromatic
  }else{
      let d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch(max){
          case r: h = (g - b) / d + (g < b ? 6 : 0); break;
          case g: h = (b - r) / d + 2; break;
          case b: h = (r - g) / d + 4; break;
      }
      h /= 6;
  }
  h = Math.floor(h * 360)
  s = Math.floor(s * 100)
  l = Math.floor(l * 100)
  return [h, s, l]
}

function makeDraggable(shape) {
  shape.draggable().on('dragstart.namespace', () => {
    clearBoundingBoxes()
  }).on('dragend.namespace', () => {
    highlightShape(shape)
 })
}

function clearBoundingBoxes () {
  let boundingBoxes = document.getElementsByClassName('highlight-wrapper')
  Array.from(boundingBoxes).forEach(box => box.remove())
}

function highlightShape(shape) {
  shape.addClass('active-shape')
  let boundingBox = shape.rbox()
  let wrapper = document.createElement("div")
  wrapper.style.position = "absolute"
  wrapper.style.left = boundingBox.x + 'px'
  wrapper.style.top = boundingBox.y + 'px'
  wrapper.style.width = boundingBox.w + 'px'
  wrapper.style.height = boundingBox.h + 'px'
  wrapper.className = 'highlight-wrapper'
  let outline = document.createElement("div")
  outline.className = 'bounding-box-outline'
  wrapper.appendChild(outline)
  let boundingBoxLayer = document.getElementById('bounding-boxes')
  boundingBoxLayer.appendChild(wrapper);
  // .highlight-wrapper element is absolute positioned
  //  .bounding-box-outline element must be relative positioned
  //    in order to add an absolute positioned :before pseudoclass
}

function openNav() {
  document.getElementById("sidenav-right").style.transform = "translate(0px,0px)";
}

function closeNav() {
  document.getElementById("sidenav-right").style.transform = "translate(250px,0px)";
}

export { rgbToHsl, makeDraggable, clearBoundingBoxes, highlightShape, openNav, closeNav }
